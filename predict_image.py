import json
import numpy as np
from jsonpath_rw import parse
from predict import predict_keypoints
from keras.models import load_model
from sklearn.preprocessing import Normalizer

data_dir = 'dataset/kamaes/json'
keypoint_file = 'dataset/kamaes/json/ichimonji_no_kamae/ichimonji_no_kamae_s02_keypoints.json'
# read keypoints
x = []
# read data
with open(keypoint_file) as f:
    json_text = json.load(f)
    jsonpath_expression = parse('$.people[0].pose_keypoints_2d[*]')
    pose_keypoints_2d = [match.value for match in jsonpath_expression.find(json_text)]
    pose_keypoints_2d = np.array(pose_keypoints_2d)
    keypoints = []
    # filter out confidence values and create tuples (x,y)
    if (len(pose_keypoints_2d > 0)):
        i = 0
        while i < len(pose_keypoints_2d):
            keypoints.append(pose_keypoints_2d[i])
            keypoints.append(pose_keypoints_2d[i + 1])
            i = i + 3
    x.append(keypoints)

print(predict_keypoints(x))
