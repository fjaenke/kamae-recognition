import numpy as np

def filter_from_json():
    return


def filter_from_keypoints(keypoints):
    x = []
    pose_keypoints_2d = np.array(keypoints)
    keypoints = []
    # filter out confidence values and create tuples (x,y)
    if (len(pose_keypoints_2d > 0)):
        i = 0
        while i < len(pose_keypoints_2d):
            keypoints.append(pose_keypoints_2d[i])
            keypoints.append(pose_keypoints_2d[i + 1])
            i = i + 3
    x.append(keypoints)
    return x
