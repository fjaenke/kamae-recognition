# kamae-recognition

## Quick Start
$ bin\OpenPoseDemo.exe --hand --image_dir path_to_images/ --write_json --number_people_max 1

## Kamae Sensei

Im Ordner dataset/kamae_sensei sind Bilder abgelegt, in denen ein Sensei 3 Kamae durchführt. Aufgenommen wurde aus verschiedenen Blickwinkeln.

Die Bilder wurden nachbearbeitet: Das Gesicht wurde unkenntlich gemacht und weitere Personen im Hintergrund stark verpixelt.

Die Bilder wurden jeweils gespiegelt.

### Verwenden mit OpenPose

Folgender Befehl wurde in OpenPoseDemo ausgeführt:

`bin\OpenPoseDemo.exe --hand --image_dir path_to_images/ --write_json path_to_write_json --number_people_max 1 --write_images path_to_write_images/`

Die Ergebnisse wurden in den Ordnern out/images und out/json abgelegt.

#### Probleme bei der Verwendung von OpenPose

- Hände werden teils nicht erkannt
- Füße werden auf Schatten gemappt
- verdeckete Beine und Arme werden falsch gemappt

## Input Data Structure

- label
    - sequence
        - frames
``` x = frames[]
``` frames[] = keyframes[]
``` keyframes[] = (x,y)