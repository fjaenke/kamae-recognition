# From Python
# It requires OpenCV installed for Python
import sys
import cv2
import os
from sys import platform
import argparse


# Paths that point to a dependent directory of your openpose build
dir_path = os.path.dirname(os.path.realpath(__file__))
python_wrapper_path = dir_path + '\\clone_openpose_here\\openpose\\build\\python\\openpose\\Release'
x64_release_path = dir_path + '\\clone_openpose_here\\openpose\\build\\x64\\Release'
bin_path = dir_path + '\\clone_openpose_here\\openpose\\build\\bin'
model_path = dir_path + '\\clone_openpose_here\\openpose\\models'

try:
    try:
        sys.path.append(python_wrapper_path)
        # semicolons are required as separators between all path variables
        os.environ['PATH'] = os.environ['PATH'] + ";" + x64_release_path + ';' + bin_path + ';'
        import pyopenpose as op
    except ImportError as e:
        print(
            'OpenPose library could not be found. Did you enable `BUILD_PYTHON` in CMake and have this Python script in the right folder?')
        raise e

    # Flags
    parser = argparse.ArgumentParser()
    parser.add_argument("--camera")
    args = parser.parse_known_args()

    # Custom Params (refer to include/openpose/flags.hpp for more parameters)
    params = dict()
    params["model_folder"] = model_path

    cap = cv2.VideoCapture(0)

    # Starting OpenPose
    opWrapper = op.WrapperPython()
    opWrapper.configure(params)
    opWrapper.start()

    while True:
        # Capture frame-by-frame
        ret, frame = cap.read()
        # Our operations on the frame come here

        datum = op.Datum()
        datum.cvInputData = frame

        opWrapper.emplaceAndPop([datum])

        print("Body keypoints: \n" + str(datum.poseKeypoints))
        cv2.imshow("OpenPose 1.5.1 - Tutorial Python API", datum.cvOutputData)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    # When everything done, release the capture
    cap.release()
    cv2.destroyAllWindows()
except Exception as e:
    print(e)
    sys.exit(-1)
