import numpy as np
from keras.callbacks import TensorBoard, ModelCheckpoint
from keras.layers import Dense, LSTM
from keras.models import Sequential
from sklearn import preprocessing
from load_data import load_data,slice_data

# Aus Datensatzverzeichnis, json kata's training und testdatensatz laden
x, y = load_data('dataset\\katas\\json\\train')
x_test, y_test = load_data('dataset\\katas\\json\\test')

# Samples mit einem Sliding-Window aufteilen
x, y = slice_data(x, y, 30)
x_test, y_test = slice_data(x_test, y_test, 30)

# Anzahl der Klassen bestimmen
classes = len(np.unique(y))

# Labels binär kodieren
lb = preprocessing.LabelBinarizer()
y = lb.fit_transform(y)
y_test = lb.fit_transform(y_test)

# Model festlegen
model = Sequential()
model.add(LSTM(50, input_shape=(30, 50)))
model.add(Dense(classes, activation='softmax'))

# Kompiliert Model, legt Verlustfunktion, Optimierer und zu optimierende Metrik fest
model.compile(
    loss='categorical_crossentropy',
    metrics=['accuracy'],
    optimizer='adam'
)

# Erstellt log files für Tensorboard
tb_callback = TensorBoard(
    log_dir='logs\\',
)

# Speichert Model wenn val_acc ein neues Maximum erreicht
ckpt_callback = ModelCheckpoint(
    filepath='models\\kata-model.h5',
    save_best_only=True,
    monitor='val_acc',
    mode='max',
    verbose=1
)

# start training
model.fit(
    x,
    y,
    validation_data=(x_test, y_test),
    batch_size=1,
    epochs=1000,
    callbacks=[
        tb_callback,
        ckpt_callback
    ]
)
