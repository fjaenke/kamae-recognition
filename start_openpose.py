# From Python
# It requires OpenCV installed for Python
import argparse
import os
import sys
import time

import cv2
import numpy as np

from filter import filter_from_keypoints
from predict import predict_keypoints

# Paths that point to a dependent directory of your openpose build
dir_path = os.path.dirname(os.path.realpath(__file__))
python_wrapper_path = dir_path + '\\clone_openpose_here\\openpose\\build\\python\\openpose\\Release'
x64_release_path = dir_path + '\\clone_openpose_here\\openpose\\build\\x64\\Release'
bin_path = dir_path + '\\clone_openpose_here\\openpose\\build\\bin'
model_path = dir_path + '\\clone_openpose_here\\openpose\\models'

try:
    try:
        sys.path.append(python_wrapper_path)
        # semicolons are required as separators between all path variables
        os.environ['PATH'] = os.environ['PATH'] + ";" + x64_release_path + ';' + bin_path + ';'
        import pyopenpose as op
    except ImportError as e:
        print(
            'OpenPose library could not be found. Did you enable `BUILD_PYTHON` in CMake and have this Python script in the right folder?')
        raise e

    # Flags
    parser = argparse.ArgumentParser()
    parser.add_argument("--image_dir",
                        default="C:\\Users\\Felix\\Documents\\PyCharm\\kamae-recognition\\dataset\\kamaes\images\\hira_no_kamae",
                        help="Process a directory of images.")
    parser.add_argument("--no_display", default=False, help="Enable to disable the visual display.")
    parser.add_argument("--num_gpu", default=op.get_gpu_number(), help="Number of GPUs.")
    args = parser.parse_known_args()

    # Custom Params (refer to include/openpose/flags.hpp for more parameters)
    params = dict()
    params["model_folder"] = model_path
    params["num_gpu"] = int(vars(args[0])["num_gpu"])
    numberGPUs = int(params["num_gpu"])

    # Add others in path? (does construct a command as in openpose -image -hand etc.)
    for i in range(0, len(args[1])):
        curr_item = args[1][i]
        if i != len(args[1]) - 1:
            next_item = args[1][i + 1]
        else:
            next_item = "1"
        if "--" in curr_item and "--" in next_item:
            key = curr_item.replace('-', '')
            if key not in params:
                params[key] = "1"
        elif "--" in curr_item and "--" not in next_item:
            key = curr_item.replace('-', '')
            if key not in params:
                params[key] = next_item

    # Starting OpenPose
    opWrapper = op.WrapperPython()
    opWrapper.configure(params)
    opWrapper.start()

    # Read frames on directory
    imagePaths = op.get_images_on_directory(args[0].image_dir)

    # Read number of GPUs in your system
    start = time.time()

    # Process and display images
    for imageBaseId in range(0, len(imagePaths), numberGPUs):

        # Create datums
        datums = []
        images = []

        # Read and push images into OpenPose wrapper
        for gpuId in range(0, numberGPUs):

            imageId = imageBaseId + gpuId
            if imageId < len(imagePaths):
                imagePath = imagePaths[imageBaseId + gpuId]
                datum = op.Datum()
                images.append(cv2.imread(imagePath))
                datum.cvInputData = images[-1]
                datums.append(datum)
                opWrapper.waitAndEmplace([datums[-1]])

        # Retrieve processed results from OpenPose wrapper
        for gpuId in range(0, numberGPUs):

            imageId = imageBaseId + gpuId
            if imageId < len(imagePaths):

                datum = datums[gpuId]
                opWrapper.waitAndPop([datum])

                keypoints = datum.poseKeypoints[0]
                keypoints = keypoints.reshape(-1, 1)
                keypoints = filter_from_keypoints(keypoints)
                result = predict_keypoints(keypoints)[0]
                classes = ['hira_no_kamae', 'ichimonji_no_kamae', 'shoshinsha_no_kamae']
                # print("Body keypoints: \n" + str(keypoints))
                print('Truth:', imagePath, 'Predicted:', classes[np.argmax(result)])

                if not args[0].no_display:
                    cv2.imshow("OpenPose 1.5.1 - Tutorial Python API", datum.cvOutputData)
                    key = cv2.waitKey(15)
                    if key == 27:
                        break

    end = time.time()
    print("OpenPose demo successfully finished. Total time: " + str(end - start) + " seconds")
except Exception as e:
    print(e)
    sys.exit(-1)
