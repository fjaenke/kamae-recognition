import numpy as np
import seaborn as sn
import matplotlib.pyplot as plt
from keras.engine.saving import load_model
from pandas import DataFrame
from load_data import load_data,slice_data
from sklearn.metrics import confusion_matrix

# Aus Datensatzverzeichnis, json kata's training und testdatensatz laden
x_test, y_test = load_data('dataset\\katas\\json\\test')

# Samples mit einem Sliding-Window aufteilen
x_test, y_test = slice_data(x_test, y_test, 30)

# Lade Model
model = load_model('models\\kata-model.h5')
model.compile(
        loss='categorical_crossentropy',
        metrics=['accuracy'],
        optimizer='adam'
)

actual = y_test
predicted = []
katas = ['chi_no_kata','fu_no_kata','ka_no_kata','ku_no_kata','sui_no_kata']

for timeframe in x_test:
    timeframe = np.reshape(timeframe, (1, 30, 50))
    prediction = model.predict(timeframe)
    predicted.append(katas[np.argmax(prediction[0])])

confm = confusion_matrix(actual, predicted)
df_cm = DataFrame(confm, index=katas, columns=katas)
ax = sn.heatmap(df_cm, cmap='Oranges', annot=True, fmt='g')
plt.show()