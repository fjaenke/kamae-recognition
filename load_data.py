import json
import os
import numpy as np
from jsonpath_rw import parse
from sklearn.preprocessing import Normalizer


# read data
def load_data(train_dir):
    x = []
    y = []
    for subdir, subdirList, files in os.walk(train_dir):
        subdirs = os.path.split(subdir)[0]
        label = os.path.split(subdirs)[1]
        frames = []
        for file in files:
            fullPath = subdir + '\\' + file
            with open(fullPath) as f:
                json_text = json.load(f)
                jsonpath_expression = parse('$.people[0].pose_keypoints_2d[*]')
                pose_keypoints_2d = [match.value for match in jsonpath_expression.find(json_text)]
                pose_keypoints_2d = np.array(pose_keypoints_2d)
                keypoints = []
                # Konfidenzen herausfiltern und xy keypoints selektieren
                if len(pose_keypoints_2d > 0):
                    i = 0
                    while i < len(pose_keypoints_2d):
                        x_keypoint = pose_keypoints_2d[i]
                        y_keypoint = pose_keypoints_2d[i + 1]
                        keypoints.append(x_keypoint.astype(np.float))
                        keypoints.append(y_keypoint.astype(np.float))
                        i = i + 3
                    keypoints = np.array(keypoints)
                    keypoints = keypoints.reshape(1, -1)
                    keypoints = Normalizer().fit_transform(keypoints)
                    frames.append(np.reshape(keypoints, 50))
        if len(frames) > 0:
            while len(frames) < 130:
                frames.append(np.zeros(50))
            x.append(np.reshape(frames, (130, 50)))
            y.append(label)
    # The input for LSTMs must be three dimensional. We can reshape the 2D sequence into a 3D sequence with 1 sample,
    # 130 timesteps, and 50 features (xy). We need to reshape the NumPy array into a format expected by the LSTM networks,
    # that is [samples, time steps, features].
    x = np.reshape(x, (len(x), len(x[0]), len(x[0][0])))
    return x, y


def slice_data(x,y,timesteps):
    i = 0
    sliced_data = []
    sliced_target = []
    # Iteriert über alle samples in x
    while i < len(x):
        # Aktuelles Sample x und dazugehörige Klasse y
        current_sample = x[i]
        current_class = y[i]
        # Iteriert über Timesteps (0 bis 100, da 130 - 30)
        j = 0
        while j < len(current_sample) - timesteps:
            # Selektiert 30 Timesteps (j + 29)
            sliced_sample = current_sample[j:j+timesteps][:]
            # Fügt sie zum Datensatz hinzu
            sliced_data.append(sliced_sample)
            sliced_target.append(current_class)
            j = j + 1
        i = i + 1
    sliced_data = np.reshape(sliced_data, (len(sliced_data), 30, 50))
    return sliced_data, sliced_target
