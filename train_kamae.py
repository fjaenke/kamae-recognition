import os
import json
import numpy as np
from jsonpath_rw import parse
from keras.models import Sequential
from keras.layers import Dense
from keras.callbacks import TensorBoard, ModelCheckpoint
from sklearn import preprocessing
from sklearn.preprocessing import Normalizer

# read data
x = []
y = []
classes = 3
data_dir = 'dataset\\kamaes\\json'
model_dir = 'models\\'
logs_dir = 'logs\\'

# read data
for subdir, subdirList, files in os.walk(data_dir):
    label = os.path.split(subdir)[1]
    # überspringt das json verzeichnis
    if(label=='json'):
        continue
    for file in files:
        fullPath = subdir + '\\' + file
        with open(fullPath) as f:
            json_text = json.load(f)
            jsonpath_expression = parse('$.people[0].pose_keypoints_2d[*]')
            pose_keypoints_2d = [match.value for match in jsonpath_expression.find(json_text)]
            pose_keypoints_2d = np.array(pose_keypoints_2d)
            keypoints = []
            # filter out confidence values and create tuples (x,y)
            if (len(pose_keypoints_2d > 0)):
                i = 0
                while i < len(pose_keypoints_2d):
                    keypoints.append(pose_keypoints_2d[i])
                    keypoints.append(pose_keypoints_2d[i + 1])
                    i = i + 3
            x.append(keypoints)
            y.append(label)
            print('Added:', label, file, 'with values:', len(keypoints))

# label binarize
lb = preprocessing.LabelBinarizer()
y = lb.fit_transform(y)

# train test split
train_x = []
train_y = []

# convert x string values to float values
x = np.array(x)
l = []
for i in x:
    i = i.astype(np.float)
    l.append(i)
x = np.array(l)
x = Normalizer().fit_transform(x)

# create model
model = Sequential([
    Dense(50, activation='relu'),
    Dense(50, activation='relu'),
    Dense(classes, activation='softmax')
])

# compile model
model.compile(
    loss='categorical_crossentropy',
    metrics=['accuracy'],
    optimizer='adam'
)

# tensorboard callback
tb_callback = TensorBoard(
    log_dir=logs_dir,
)

# checkpoint callback in order to save when a new best accuracy has been achieved
ckpt_callback = ModelCheckpoint(
    filepath=model_dir+'myModel.h5',
    save_best_only=True,
)

# start training
model.fit(
    x,
    y,
    batch_size=16,
    epochs=800
    # callbacks= [
    #     tb_callback
    # ]
)

# save model
model.save(
    filepath=model_dir+'kamae-model.h5',
    overwrite=True
)
