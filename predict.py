import numpy as np
from keras.models import load_model
from sklearn.preprocessing import Normalizer


def predict_keypoints(keypoints_to_predict):
    model_dir = 'models\\'
    # preprocessing: convert x string values to float values
    keypoints_to_predict = np.array(keypoints_to_predict)
    l = []
    for i in keypoints_to_predict:
        i = i.astype(np.float)
        l.append(i)
    x = np.array(l)
    x = x.reshape(1, 50)
    # normalize, e.g. scale values in range between 0 and 1
    x = Normalizer().fit_transform(x)

    # load model
    model = load_model(model_dir+'kamae-model.h5')
    model.compile(
        loss='categorical_crossentropy',
        metrics=['accuracy'],
        optimizer='adam'
    )
    return model.predict(x)
