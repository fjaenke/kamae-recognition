# Installationsanweisung für openpose

## Vorraussetzungen  
[Git](https://git-scm.com/download/win)  
[Python 3.7](https://www.python.org/ftp/python/3.7.0/python-3.7.0.exe)  
[CMake](https://github.com/Kitware/CMake/releases/download/v3.16.2/cmake-3.16.2-win64-x64.msi)  
[Visual Studio Community](https://visualstudio.microsoft.com/thank-you-downloading-visual-studio/?sku=Community&rel=16)

## Python Abhängigkeiten
pip install numpy opencv-python

## openpose builden 
Das openpose Projekt klonen  
git clone https://github.com/CMU-Perceptual-Computing-Lab/openpose.git

im openpose Projektverzeichnis ein Verzeichnis für build anlegen  
mkdir build

cmake
1. Unter "Where is the source code" geklontes openpose Projektverzechnis
   angeben
2. Unter "Where to build the binaries" das erstellte build Verzeichnis
   angeben
3. Auf configure klicken
4. Visual Studio 16 2019 auswählen, danach Finish
4. BUILD_PYTHON Checkbox markieren
6. Generate klicken

Im build verzeichnis sollte nun ein OpenPose.sln File existieren.  
Dieses mit Visual Studio 16 2019 Community öffnen.  
Anschließend in der Menüleiste "Debug" auf "Release" umstellen.  
Danach in der Menüleiste "Erstellen" > "Projektmappe erstellen"
anklicken  

Wenn der Vorgang erfolgreich war, sollte bei der Ausführung der Datei
"openpose\build\examples\tutorial_api_python\01_body_from_image.py" für
ein Bild das Posen-Skelett eingezeichnet werden.